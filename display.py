from termcolor import colored
import datetime

# Variables
tickers = ['btcusd', 'ethusd', 'bchusd', 'ltcusd', 'dshusd', 'xmrusd', 'zecusd'] # Tracking 7 things (0 - 6)
tickers_colors = {'btcusd': 'yellow', 'ltcusd': 'white', 'ethusd': 'green', 'zecusd': 'magenta', 'xmrusd': 'red', 'dshusd': 'blue', 'bchusd': 'cyan'}
tickers2 = ['btc', 'eth', 'bch', 'ltc', 'dsh', 'xmr', 'zec']


def update_coin(coin, coins, markets):
    # Display Coin
    # Symbol.
    print colored(coin.symbol + " ", tickers_colors[coin.symbol]),

    # Timestamp.
    print colored(datetime.datetime.fromtimestamp(int(coin.timestamp)).strftime('%Y-%m-%d %H:%M:%S') + " ",
                  "white"),

    # Current Price USD.
    print colored(str(coin.last_price)),

    # Current Price BTC (well actually, it's how many of these coins it takes to buy 1 BTC)
    print colored(" %.2f " % ((coins[0].last_price / coin.last_price)), "white"),

    # How many coins can one bitcoin buy of this crypto...
    print colored(" Volume: %.2f" % (coin.volume * coin.price / 1000000), "white"),

    price_change = ((coin.price - coin.last_price) / coin.last_price) * 100

    # Display a little + or - % change on price for each 10m tick
    if coin.price > coin.last_price:
        print colored("%.2f" % price_change, "green"),
    elif coin.price < coin.last_price:
        print colored("%.2f" % price_change, "red"),
    else:
        print colored(" 0", "white"),

    coin_change = coin.price - coin.last_price
    market_change = markets.total_market_cap_usd - markets.last_total_market_cap_usd

    coin_change_proportion = 0.0
    if coin_change > 0.0 and market_change > 0.0:
        coin_change_proportion = ((coin_change * coin.total_supply) / market_change) * 100

    elif coin_change < 0.0 and market_change < 0.0:
        coin_change_proportion = ((coin_change * coin.total_supply) / market_change) * 100

    elif coin_change > 0.0 and market_change < 0.0:
        coin_change_proportion = ((coin_change * coin.total_supply) / (market_change * -1)) * 100

    elif coin_change < 0.0 and market_change > 0.0:
        coin_change_proportion = ((coin_change * coin.total_supply * -1) / (market_change)) * 100

    else:
        print "Something has gone terribly wrong."

    print colored("%.2f" % coin_change_proportion, "yellow")



def update_market(coins, markets):
    print "Bitcoin is " + str(markets.bitcoin_percentage_of_market_cap) + "% of total market cap"

    my_market_cap_total = 0.0
    for coin in coins:
        my_market_cap_total = my_market_cap_total + coin.market_cap_usd

    # calculate for % of total market cap
    my_market_cap_total = (my_market_cap_total / markets.total_market_cap_usd) * 100

    print "Your selected coins account for %.2f" % my_market_cap_total
    print "Total Market Cap: $%.2f" % (markets.stats["total_market_cap_usd"] / 1000000000.0),
    print "B"
    print "Total Market Change of %.2f" % float((markets.total_market_cap_usd - markets.last_total_market_cap_usd) / 1000000000.0)
    print "Total 24h Volume (USD): %.2f" % float(markets.total_volume_24h_usd / 1000000.0)