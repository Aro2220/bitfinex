#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Core Modules
import time

coins = []

# My Modules
import coin
import market
import display

if __name__ == '__main__':
    print "Initializing",
    coins = []
    print ".",

    markets = market.Market()
    print ".",

    # Creating coins...
    coins.append(coin.Coin('btcusd', markets))
    coins.append(coin.Coin('ethusd', markets))
    coins.append(coin.Coin('bchusd', markets))
    coins.append(coin.Coin('ltcusd', markets))
    coins.append(coin.Coin('dshusd', markets))
    coins.append(coin.Coin('xmrusd', markets))
    coins.append(coin.Coin('zecusd', markets))
    print "."

    # Main / Active loop
    while 1:

        # Update coinmarkethub
        markets.update()

        # Update bitfinex
        # TODO: Have this update more frequently than coinmarkethub (based on maximum allowable poll speeds for each - will need to test)
        for coin in coins:
            coin.update(markets)
            #Display individual coin stats
            display.update_coin(coin, coins, markets)

        # Display Overall Stats
        display.update_market(coins, markets)


        time.sleep(180) # how many seconds to wait
