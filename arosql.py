# encoding=utf8

import mysql.connector
import imp
import os

class AroSQL:
    # Importing password file
    
    def __init__(self):
        self.sql_update = 1 # Default ON - will turn off on error...
        self.path = os.path.join(os.path.expanduser('~'), 'secretBitSQL.py') # change as needed
        # if os.name != "nt": # If I want to disable Windows SQL operation by default...
        try:
            self.secret = imp.load_source('secretBitSQL.py', self.path)
        except IOError:
            print "SQL login data missing, disabling SQL"
            self.sql_update = 0

        self.cnx = None
        self.cursor = None

    def connect(self):
        if self.sql_update == 1:
            try:
                self.cnx = mysql.connector.connect(**self.secret.config) # login details are in the bitSQL.py file
                self.cursor = self.cnx.cursor(buffered=True)
            except mysql.connector.errors.InterfaceError:
                print "MySQL connection error! - VPS too slow?"



    def close(self):
        if self.sql_update == 1:
            self.cursor.close()
            self.cnx.disconnect()

    def execute(self, cmd):
        if self.sql_update == 1:
            self.connect()
            self.cursor.execute(cmd)

            self.close()
            return self.cursor


    def updateDB(self, coin, checktime, price, volume):
        if self.sql_update == 1:
            self.connect()

            # Just insert the new data...no need to check it.
            query = ("INSERT INTO History (Coin, Time, Price, Volume) VALUES ('" + str(coin) + "', '" + str(checktime) + "', '" + str(price) + "', '" + str(volume) + "')")

            self.cursor.execute(query)
            self.cnx.commit()