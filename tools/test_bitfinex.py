#!/usr/bin/env python
# -*- coding: utf-8 -*-

# test refresh speed of coinmarkethub

import coin
import market
import datetime
import time

if __name__ == '__main__':
    markets = market.Market()

    coins = []
    # Creating coins...
    #coins.append(coin.Coin('btcusd', markets))
    #coins.append(coin.Coin('ethusd', markets))
    coins.append(coin.Coin('bchusd', markets))
    #coins.append(coin.Coin('ltcusd', markets))
    #coins.append(coin.Coin('dshusd', markets))
    #coins.append(coin.Coin('xmrusd', markets))
    #coins.append(coin.Coin('zecusd', markets))

    now = datetime.datetime.now()
    last_now = now
    start_time = datetime.datetime.now()
    cycles = 0

    market_cap = markets.total_market_cap_usd
    last_market_cap = market_cap
    average = []

    # Main / Active loop
    while 1:
        cycles += 1

        markets.update()

        # Update bitfinex
        for coin in coins:
            coin.update(markets)

        time.sleep(1)

        if coin.price != coin.last_price:
            difference = coin.price - coin.last_price
            last_now = now
            now = datetime.datetime.now()

            average.append((now - last_now).seconds)
            total = 0
            count = 0
            for avg in average:
                total = total + avg
                count += 1
            real_average = total / count

            print "Last cycle: " + str((now - last_now).seconds) + " seconds for a total elapsed time of " + str(
                ((now - start_time).seconds) / 60) + " minutes. D = " + str(difference) + " and total average of " + str(real_average)


