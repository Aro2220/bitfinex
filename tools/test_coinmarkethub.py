#!/usr/bin/env python
# -*- coding: utf-8 -*-

# test refresh speed of coinmarkethub

import market
import datetime
import time

if __name__ == '__main__':
    now = datetime.datetime.now()
    last_now = now
    start_time = datetime.datetime.now()
    cycles = 0
    markets = market.Market()
    market_cap = markets.total_market_cap_usd
    last_market_cap = market_cap


    # Main / Active loop
    while 1:
        cycles += 1

        # Update coinmarkethub
        markets.update()

        market_cap = markets.total_market_cap_usd

        time.sleep(1)

        if last_market_cap != market_cap:
            difference = market_cap - last_market_cap
            last_market_cap = market_cap
            last_now = now
            now = datetime.datetime.now()
            print "Last cycle: " + str((now - last_now).seconds) + " seconds for a total elapsed time of " + str(
                ((now - start_time).seconds) / 60) + " minutes. D = " + str(difference)


