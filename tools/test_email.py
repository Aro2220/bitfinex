import smtplib # for send_email
import imp
import os

# Import the email modules we'll need
from email.mime.text import MIMEText

# Added compatibility for Windows/Linux pathing
if os.name == "nt":
    secret = imp.load_source('secretSQL', 'Y:\\Projects\\Python\\secretContact.py')
else:
    secret = imp.load_source('secretSQL', '/home/aro/secretContact.py')

def send_email(msg="There was a terrible error and I just wanted you to know."):
    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    s = smtplib.SMTP('localhost')
    s.sendmail(secret.fromaddr, secret.toaddrs, msg)
    s.quit()

send_email()





