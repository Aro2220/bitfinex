SPECIFIC VERSIONS REQUIRED:

pip install coinmarketcap==3.0.1 (version 4.0 gives me weird request_cache errors)
pip install mysql-connector==2.1.4

ticker - ask, timestamp, bid, last_price, mid
today - high, volume, low
stats - bitcoin_percentage_of_market_cap, total_market_cap_usd, active_markets, active_assets, active_currencies, total_24h_volume_usd,
tick - market_cap_usd, price_usd, last_updated, name, 24h_volume_usd, percent_change_7d, symbol, rank, percent_change,
       total_supply, price_btc, available_supply, percent_change_24h, id,    ..each is in order.

Just migrated to Bitbucket.
