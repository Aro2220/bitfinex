#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program will notify the user by text message or send_email. It can also write to a log file.

#from twilio.rest import TwilioRestClient # for text messages
import smtplib # for send_email
import imp
import os
import time
import datetime
import codecs # for writing to a file in utf-8

# Added compatibility for Windows/Linux pathing
if os.name == "nt":
    secret = imp.load_source('secretSQL', 'Y:\\Projects\\Python\\secretContact.py')
else:
    secret = imp.load_source('secretSQL', '/home/aro/secretContact.py')

def gmail(msg="There was a terrible error that occured and I wanted you to know!"):
    
    server = smtplib.SMTP_SSL('smtp.gmail.com:465')
    server.ehlo()
    server.login(secret.username, secret.password)
    server.sendmail(secret.fromaddr, secret.toaddrs, msg)
    server.quit()
    print "Alert: " + msg,

def send_email(msg="There was a terrible error and I just wanted you to know."):
    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    s = smtplib.SMTP('localhost')
    s.sendmail(secret.fromaddr, secret.toaddrs, msg)
    s.quit()
    print msg

'''
# Must pay $1/mo for a line and then a fraction of a cent per SMS (receiving or sending)at Twilio to activate this feature.
def sms(msg="There was a terrible error that occured and I wanted you to know!"):
    client = TwilioRestClient(secret.account_sid, secret.auth_token)
    # this IMMEDIATELY sends the message!
    message = client.messages.create(
    body=msg,
    to=secret.phone_to,
    from_=secret.phone_from
    )
    # print some info to the console
    print message.sid,
    print "Alert: " + msg,
'''

def log(msg="There was a terrible error that occured and I wanted you to know!"):
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

    # using codecs so file is written in utf-8 and not ascii, as some game names have non-ascii character points.
    with codecs.open("error.log", mode="a", encoding="utf8") as f:
        f.write(timestamp)
        f.write(": ")
        f.write(msg)
        f.write("\n")
        f.close()

    # print msg was causing encoding problems on my linux terminal...
    utf_msg = msg.encode('utf-8')
    print utf_msg
