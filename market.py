# This class will store all main information about the coinmarkethub connection as a whole.

# 3rd party modules
# TODO: Maybe can do more connections with websocket
# TODO: https://github.com/nlsdfnbch/btfxwss
import coinmarketcap

class Market():
    # Create classes for data access
    cmc = coinmarketcap.Market()  # Coinmarket Cap - overall data

    def __init__(self):

        self.stats = Market.cmc.stats()

        self.total_market_cap_usd = self.stats["total_market_cap_usd"]
        self.active_markets = self.stats["active_markets"]
        self.total_volume_24h_usd = self.stats["total_24h_volume_usd"]
        self.active_currencies = self.stats["active_currencies"]
        self.bitcoin_percentage_of_market_cap = self.stats["bitcoin_percentage_of_market_cap"]

        self.update()

    def update(self):

        self.stats = Market.cmc.stats()

        self.last_total_market_cap_usd = self.total_market_cap_usd
        self.total_market_cap_usd = self.stats["total_market_cap_usd"]

        self.last_active_markets = self.active_markets
        self.active_markets = self.stats["active_markets"]

        self.last_total_volume_24h_usd = self.total_volume_24h_usd
        self.total_volume_24h_usd = self.stats["total_24h_volume_usd"]

        self.last_active_currencies = self.active_currencies
        self.active_currencies = self.stats["active_currencies"]

        self.last_bitcoin_percentage_of_market_cap = self.bitcoin_percentage_of_market_cap
        self.bitcoin_percentage_of_market_cap = self.stats["bitcoin_percentage_of_market_cap"]



