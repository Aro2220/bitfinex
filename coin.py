import datetime
import requests

import coinmarketcap
import bitfinex

import arosql
import alert

coins = []
total_market_cap = 0
last_total_market_cap = 0

# Create classes for data access
coinmarketcap = coinmarketcap.Market()  # Coinmarket Cap - overall data
bitfinex = bitfinex.client.Client()  # Bitfinex current trading data`

mysql = arosql.AroSQL()

class Coin:
    """A cryptocurrency"""

    def __init__(self, symbol, markets, price_watch_high = 0, price_watch_low = 0):
        print "New coin init: " + symbol
        coins.append(self)
        self.symbol = symbol

        self.ticker = bitfinex.ticker(self.symbol)
        self.today = bitfinex.today(self.symbol) # only for 24h volume stat...not super useful
        self.price = self.ticker['last_price']

        # Warning settings for this coin
        # TODO: Change this to grab the data from SQL so I can just update the SQL to change this.
        if price_watch_high == 0:
            self.price_watch_high = self.price * 1.05
        if price_watch_low == 0:
            self.price_watch_low = self.price * 0.95

        # Stuff from Coinmarketcap
        index = 0
        for market in markets.cmc.ticker():
            if market['symbol'].lower() == self.symbol[:3]:
                self.index = index
                self.id = market['id']
                self.name = market['name']
                self.rank = market['rank']
                self.market_cap_usd = float(market['market_cap_usd'])
                self.total_supply = float(market['total_supply'])
                self.percent_change_1h = float(market['percent_change_1h'])
                self.percent_change_24h = float(market['percent_change_24h'])
                self.volume_24h_usd = float(market['24h_volume_usd'])
            index += 1

        self.update(markets)

    def update(self, markets):
        # Refresh all market data with error correction
        # refreshing data from websites
        try:
            self.ticker = bitfinex.ticker(self.symbol)
        except (requests.exceptions.Timeout, ValueError, requests.exceptions.ConnectionError) as e:
            print "Error with bitfinex.ticker(" + self.symbol + ") -- " + str(e)

        try:
            self.today = bitfinex.today(self.symbol)
        except (requests.exceptions.Timeout, ValueError, requests.exceptions.ConnectionError) as e:
            print "Error with bitfinex.today(" + self.symbol + ") -- " + str(e)


        # Stuff from Bitfinex
        self.last_price = self.price
        self.price = self.ticker['last_price']
        self.timestamp = self.ticker['timestamp']
        self.volume = self.today[u'volume']

        # Stuff from Coinmarketcap
        index = 0
        for market in markets.cmc.ticker():
            if market['symbol'].lower() == self.symbol[:3]:
                self.last_index = self.index
                self.index = index

                self.last_id = self.id
                self.id = market['id']

                self.last_name = self.name
                self.name = market['name']

                self.last_rank = self.rank
                self.rank = int(market['rank'])

                self.last_market_cap_usd = self.market_cap_usd
                self.market_cap_usd = float(market['market_cap_usd'])

                self.last_total_supply = self.total_supply
                self.total_supply = float(market['total_supply'])

                self.last_percent_change_1h = self.percent_change_1h
                self.percent_change_1h = float(market['percent_change_1h'])

                self.last_percent_change_24h = self.percent_change_24h
                self.percent_change_24h = float(market['percent_change_24h'])

                self.last_volume_24h_usd = self.volume_24h_usd
                self.volume_24h_usd = float(market['24h_volume_usd'])
                index += 1
                break
            index += 1

        # Separate
        self.monitor()
        # Separate
        self.updateSql(markets)


    def monitor(self):
        standard_deviation = 0.02

        # Alert me by send_email when the price goes up or down more than 5%.
        if self.price >= self.price_watch_high:
            alert.send_email("WARNING! PRICE WATCH HIGH TRIGGERED FOR " + self.symbol + " AT $" + str(self.price))
            self.price_watch_high = self.price * (1.0 + standard_deviation)
        if self.price <= self.price_watch_low:
            alert.send_email("WARNING! PRICE WATCH LOW TRIGGERED FOR " + self.symbol + " AT $" + str(self.price))
            self.price_watch_low = self.price * (1.0 - standard_deviation)


    def updateSql(self, markets):
        # Update new values into SQL DB
        mysql.updateDB(self.symbol,
                       datetime.datetime.fromtimestamp(int(self.timestamp)).strftime('%Y-%m-%d %H:%M:%S'),
                       self.last_price, self.volume * self.last_price / 1000000)